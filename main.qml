import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

Window {
    visible: true
    width: 640
    height: 480
    color: "#d2d4dc"

    Label {
        anchors.centerIn: parent
        text: "Hello embendded world!"
        font: {
            size: "16"
        }
    }
}

